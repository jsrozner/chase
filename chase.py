#!/usr/local/bin/python
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import calendar
import getpass
import os
import re

kChaseLoginLink = "https://www.chase.com/"
kUserName = "defaultuser"
kCookieFile = "cookies.txt"
kStatementsDir = "defaultdir"
kConfigFile = "config.txt"
kLocalConfig = "localconfig.txt"
kTmpConfigFile = "config.tmp"

def writeCookiesToFile(driver):
  cookiefile = open("./" + kCookieFile, "w");

  for cookie in driver.get_cookies():
    expiry = cookie.get('expiry', "0")
    cookiefile.write("%s\tTRUE\t%s\t%s\t%s\t%s\t%s\n" % (cookie['domain'],
        cookie['path'], cookie['secure'], expiry, cookie['name'], cookie[
        'value']))

  cookiefile.close()

def login(driver):
  driver.get(kChaseLoginLink)

  # Login
  driver.find_element_by_xpath('//*[@id="usr_name_home"]').send_keys(kUserName)
  driver.find_element_by_xpath('//*[@id="usr_password_home"]').send_keys(getpass.getpass())
  # Click login
  driver.find_element_by_xpath('//*[@id="primary-hero"]/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/div/form/div[1]/div/div[5]/div/div/a').click()

  try:
    WebDriverWait(driver, 10).until(
      EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, "statements")))
  except:
    driver.quit()

def downloadStatements(driver, newconfigFile, account, account_link,
                       start_year,
                       start_month):
  driver.get(account_link)
  # The below is necessary when a new year occurs.
  #getpass.getpass("hit enter when statement links are visible")
  month = start_month
  year = start_year
  downloaded = 0

  while(1):
    month_text = calendar.month_name[month]
    month_spaced = "%02d" % month

    elements = driver.find_elements_by_xpath("//*[contains(text(), "
                                              "'" + month_text + "')]")
    for e in elements:
      link_text = e.get_attribute("href");
      if not link_text or not "javascript" in link_text:
        continue
      else:
        break;

    if link_text is None:
      break

    matches = re.search('\'(.*)\'', link_text)

    if matches is None:
      break

    suffix = matches.group(1)

    pdflink = "https://stmts.chase.com" + suffix

    cmd = 'wget --quiet -r --load-cookies cookies.txt --no-directories -P ' \
          'downloaded "'+ pdflink + '" >& /dev/null'
    print cmd
    os.system(cmd);
    os.system("./cleanup.sh " + str(year) + " " + str(month_spaced))
    os.system("mv downloaded/* " + kStatementsDir + str(account) + "/")

    downloaded += 1

    if month == 12:
      month = 1
      year += 1
      break;
    else:
      month += 1

  print "downloaded " + str(downloaded) + " files, up to " \
        + month_text
  newconfigFile.write("%s\t%s\t%s\t%s\n" % (account, account_link, year,
                                            month))

def main():
  localConfig = open("./" + kLocalConfig)
  kUserName = localConfig.readline()
  kStatementsDir = localConfig.readline()
  localConfig.close()

  if kStatementsDir == "defaultdir":
    print "please modify the configuration to specify a valid directory and " \
          "user"
    exit()
  else:
    print "username is " + kUserName
    print "directory is " + kStatementsDir

  driver = webdriver.Chrome()
  login(driver)

  writeCookiesToFile(driver)

  configFile = open("./" + kConfigFile)
  newconfigFile = open("./" + kTmpConfigFile, "w")

  for line in configFile:
    print line
    info = re.split("\\s", line)
    downloadStatements(driver, newconfigFile, info[0], info[1], int(info[2]),
                       int(info[3]))

  configFile.close()
  newconfigFile.close()

  os.system("mv " + kTmpConfigFile + " " + kConfigFile)
  os.system("rm " + kCookieFile)

  driver.quit()


main()
